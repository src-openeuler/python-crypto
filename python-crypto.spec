Name:		python-crypto
Version:	2.6.1
Release:	30
Summary:	Cryptography library for Python
License:	Public Domain and Python
URL:		http://www.pycrypto.org/
Source0:	http://ftp.dlitz.net/pub/dlitz/crypto/pycrypto/pycrypto-%{version}.tar.gz
Patch6000:	python-crypto-2.4-optflags.patch
Patch6001:	python-crypto-2.4-fix-pubkey-size-divisions.patch
Patch6002:	pycrypto-2.6.1-CVE-2013-7459.patch
Patch6003:	pycrypto-2.6.1-unbundle-libtomcrypt.patch
Patch6004:	python-crypto-2.6.1-link.patch
Patch6005:	pycrypto-2.6.1-CVE-2018-6594.patch
Patch6006:	pycrypto-2.6.1-use-os-random.patch
Patch6007:	pycrypto-2.6.1-drop-py2.1-support.patch
Patch6008:      python-crypto-2.6.1-python3.10.patch
Patch6009:      python-crypto-2.6.1-python3.11.patch
Patch6010:      python-crypto-2.6.1-python3only.patch
Patch6011:      python-crypto-2.6.1-no-distutils.patch
Patch6012:      python-crypto-2.6.1-SyntaxWarning.patch

BuildRequires:	coreutils findutils gcc %{_bindir}/2to3
BuildRequires:	gmp-devel libtomcrypt-devel python3-devel

%description
Python-crypto is a collection of both secure hash functions (such as MD5
and SHA), and various encryption algorithms (AES, DES, RSA, ElGamal, etc.).

%package -n python3-crypto
Summary:	Library for Python 3
%{?python_provide:%python_provide python3-crypto}

%description -n python3-crypto
Cryptography library for Python 3 package.

%prep
%setup -n pycrypto-%{version} -q
rm -rf src/libtom
%patch6000 -p1
%patch6001 -p1
%patch6002 -p1
%patch6003
%patch6004
%patch6005
%patch6006
%patch6007
%patch6008
%patch6009
%patch6010
%patch6011
%patch6012

cp pct-speedtest.py pct-speedtest3.py
2to3 -wn pct-speedtest3.py

%build
%global optflags %{optflags} -fno-strict-aliasing
%py3_build

%install
%py3_install

find %{buildroot}%{python3_sitearch} -name '*.so' -exec chmod -c g-w {} \;

%check
%{__python3} setup.py test

PYTHONPATH=%{buildroot}%{python3_sitearch} %{__python3} pct-speedtest3.py

%files -n python3-crypto
%license COPYRIGHT LEGAL/
%doc README TODO ACKS ChangeLog Doc/
%{python3_sitearch}/Crypto/
%{python3_sitearch}/pycrypto-%{version}-py3.*.egg-info

%changelog
* Tue Aug 30 2022 dillon chen<dillon.chen@gmail.com> - 2.6.1-30
- rename patch6008
- add patch 6009,6010,6011,6012

* Wed Jan 05 2022 shixuantong<shixuantong@huawei.com> - 2.6.1-29
- Fix Python 3.10 compatibility

* Tue Feb 09 2021 shixuantong<shixuantong@huawei.com> - 2.6.1-28
- delete python2-devel from BuildRequires 

* Tue Feb 09 2021 shixuantong<shixuantong@huawei.com> - 2.6.1-27
- remove python2-crypto

* Wed Jun 24 2020 wenzhanli<wenzhanli2@huawei.com> - 2.6.1-26
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Repair make test error

* Sat Oct 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.6.1-25
- Type:NA
- ID:NA
- SUG:NA
- DESC:Package Init
